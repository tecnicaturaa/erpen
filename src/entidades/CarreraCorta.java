package app.entidades;


public class CarreraCorta extends Carrera {

	private Integer plazoMaximo;
	private Integer cupoMinimo;
	private String regional;
	
	public CarreraCorta() {
		// TODO Auto-generated constructor stub
	}
	
	public Integer getPlazoMaximo() {
		return plazoMaximo;
	}

	public void setPlazoMaximo(Integer plazoMaximo) {
		this.plazoMaximo = plazoMaximo;
	}

	public Integer getCupoMinimo() {
		return cupoMinimo;
	}

	public void setCupoMinimo(Integer cupoMinimo) {
		this.cupoMinimo = cupoMinimo;
	}
	
	public String getRegional() {
		return regional;
	}
	
	public void SetRegional(String regional) {
		this.regional = regional;
	}
	
}
