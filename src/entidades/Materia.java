package app.entidades;

import java.util.List;

public class Materia {
	private Integer id;
	private String nombre;
	private Integer anyo;
	private TipoModalidad tipoModalidad;
	private List<Materia> listaMateriasCorrelativas;
	
	public Materia() {
		// TODO Auto-generated constructor stub
	}
	
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Integer getAnyo() {
		return anyo;
	}

	public void setAnyo(Integer anyo) {
		this.anyo = anyo;
	}

	public TipoModalidad getTipoModalidad() {
		return tipoModalidad;
	}

	public void setTipoModalidad(TipoModalidad tipoModalidad) {
		this.tipoModalidad = tipoModalidad;
	}

	public List<Materia> getListaMateriasCorrelativas() {
		return listaMateriasCorrelativas;
	}

	public void setListaMateriasCorrelativas(List<Materia> listaMateriasCorrelativas) {
		this.listaMateriasCorrelativas = listaMateriasCorrelativas;
	}
	
	
}
