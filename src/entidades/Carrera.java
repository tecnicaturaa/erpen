package app.entidades;

import java.util.List;

public class Carrera {

	private Integer id;
	private String nombre;
	private String tipoDeCarrera;
	private List<String> listaCompetencias;
	private String planDeEstudio;
	private String listaAlumnos;
	private Integer duracion;
	private CarreraRegional carreraRegional;
	
	
	public Carrera() {
		// TODO Auto-generated constructor stub
	}
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String tipoDeCarrera() {
		return tipoDeCarrera;
	}
	
	public void settipoDeCarrera(String tipoDeCarrera) {
		this.tipoDeCarrera = tipoDeCarrera;
	}
	
	public String planDeEstudio() {
		return planDeEstudio;
	}
	
	public void planDeEstudio(String planDeEstudio) {
		this.planDeEstudio = planDeEstudio;
	}
	
	public Integer getDuracion() {
		return duracion;
	}

	public void setDuracion(Integer duracion) {
		this.duracion = duracion;
	}

	public CarreraRegional getCarreraRegional() {
		return carreraRegional;
	}

	public void setCarreraRegional(CarreraRegional carreraRegional) {
		this.carreraRegional = carreraRegional;
	}

	public List<String> getListaCompetencias() {
		return listaCompetencias;
	}

	public void setListaCompetencias(List<String> listaCompetencias) {
		this.listaCompetencias = listaCompetencias;
	}
	
}
