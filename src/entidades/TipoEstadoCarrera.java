package app.entidades;

public enum TipoEstadoCarrera {
	REGULAR,
	LIBRE,
	EN_INGRESO,
	EGRESADO;
}
