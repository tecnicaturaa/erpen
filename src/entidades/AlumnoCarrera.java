package app.entidades;

public class AlumnoCarrera {

	private Alumno alumno;
	private Carrera carrera;
	private TipoEstadoCarrera tipoEstadoCarrera;
	
	public AlumnoCarrera() {
		// TODO Auto-generated constructor stub
	}

	public Alumno getAlumno() {
		return alumno;
	}

	public void setAlumno(Alumno alumno) {
		this.alumno = alumno;
	}

	public Carrera getCarrera() {
		return carrera;
	}

	public void setCarrera(Carrera carrera) {
		this.carrera = carrera;
	}

	public TipoEstadoCarrera getTipoEstadoCarrera() {
		return tipoEstadoCarrera;
	}

	public void setTipoEstadoCarrera(TipoEstadoCarrera tipoEstadoCarrera) {
		this.tipoEstadoCarrera = tipoEstadoCarrera;
	}
}
