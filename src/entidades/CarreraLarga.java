package app.entidades;

public class CarreraLarga extends Carrera {

	private Integer cupoMaximo;
	private TipoGrado tipoGrado;
	
	public CarreraLarga() {
		// TODO Auto-generated constructor stub
	}

	public Integer getCupoMaximo() {
		return cupoMaximo;
	}

	public void setCupoMaximo(Integer cupoMaximo) {
		this.cupoMaximo = cupoMaximo;
	}

	public TipoGrado getTipoGrado() {
		return tipoGrado;
	}

	public void setTipoGrado(TipoGrado tipoGrado) {
		this.tipoGrado = tipoGrado;
	}
	
	
}
