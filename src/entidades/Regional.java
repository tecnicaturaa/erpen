package app.entidades;

public class Regional {

	private Integer id;
	private String nombre;
	private String localidad;
	private String listaCarrera;
	
	public Regional() {
		// TODO Auto-generated constructor stub
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getLocalidad() {
		return localidad;
	}

	public void setLocalidad(String localidad) {
		this.localidad = localidad;
	}
	
	public String getlistaCarrera() {
		return listaCarrera;
	}
}
