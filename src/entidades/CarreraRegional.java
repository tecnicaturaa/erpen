package app.entidades;

public class CarreraRegional {

	private Carrera carrera;
	private Regional regional;
	private PlanEstudio planEstudio;
	
	public CarreraRegional() {
		// TODO Auto-generated constructor stub
	}

	public Carrera getCarrera() {
		return carrera;
	}

	public void setCarrera(Carrera carrera) {
		this.carrera = carrera;
	}

	public Regional getRegional() {
		return regional;
	}

	public void setRegional(Regional regional) {
		this.regional = regional;
	}

	public PlanEstudio getPlanEstudio() {
		return planEstudio;
	}

	public void setPlanEstudio(PlanEstudio planEstudio) {
		this.planEstudio = planEstudio;
	}
	
	
}
